# Crowd
Example project for demonstrate crowd technic in UE

Forked from https://github.com/jetset-3566/Crowd

Test with animation optimization. Optimization by about half.

Default crowd: ![](https://i.imgur.com/Ho9Opmj.png)

With AnimSharing: ![](https://i.imgur.com/lRN7EnM.png)