// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "TP_TopDownGameMode.generated.h"

UCLASS(minimalapi)
class ATP_TopDownGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATP_TopDownGameMode();
};



